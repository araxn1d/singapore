### To run tests execute
```
mvn clean test
```

### To build application execute
```
mvn clean package
```

### To to run application execute
```
java -jar console-canvas-1.0.jar
```

After build artifact will be located in `/target` folder, so application could be executed with following command:
```
java -jar target/console-canvas-1.0.jar 
```

### Improvements that could be made
* Exception handling mechanism could be improved. In current implementation every exception
terminates application execution. It's better to have recovery mechanism that allows user to continue 
work after minor exception (after entering wrong command symbol, for example).
* There is lack of validation in some parts of code. For example, there is no check that canvas is initialized.
This should be fixed.
* Separate `Color` class should be created, since using of `char color` is less agile and non-OOP way.
* Some classes are not fully covered with test (`ConsoleFrameCanvasDecorator`, `ConsoleReader`, `BucketFillCommand`, etc.).
* Overall design and code quality could be improved. I have some thoughts about it:
  + Get rid of `System.out.println` all over the code. Unified non-static component should be created.
  + Improve `ConsoleFrameCanvasDecorator`, since implementation is hard to understand.
  + Packages structure can be improved.
  + `BasicDrawLineStrategy.Line` class usage can be avoided.

