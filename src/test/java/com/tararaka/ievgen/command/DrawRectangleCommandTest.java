package com.tararaka.ievgen.command;

import com.tararaka.ievgen.Canvas;
import com.tararaka.ievgen.ExecutionResult;
import com.tararaka.ievgen.BasicDrawLineStrategy;
import com.tararaka.ievgen.Point;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.mockito.Mockito.verify;

/**
 * @author Ievgen Tararaka
 */
@RunWith(MockitoJUnitRunner.class)
public class DrawRectangleCommandTest {
    @Mock
    private Canvas canvas;
    @Mock
    private BasicDrawLineStrategy drawLineStrategy;

    @Test
    public void execute_DrawRectangle_ByConnectingPoints() {
        DrawRectangleCommand command = new DrawRectangleCommand(canvas, drawLineStrategy, Point.of(2, 2), Point.of(10, 10));
        ExecutionResult executionResult = command.execute();

        verify(drawLineStrategy).connectPoints(canvas, 'x',
                Point.of(2, 2), Point.of(10, 2),
                Point.of(10, 10), Point.of(2, 10),
                Point.of(2, 2));

        assertThat(executionResult, is(ExecutionResult.REPAINT));
    }
}
