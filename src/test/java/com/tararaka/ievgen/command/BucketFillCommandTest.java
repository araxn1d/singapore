package com.tararaka.ievgen.command;

import com.tararaka.ievgen.Canvas;
import com.tararaka.ievgen.ExecutionResult;
import com.tararaka.ievgen.Point;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

/**
 * @author Ievgen Tararaka
 */
@RunWith(MockitoJUnitRunner.class)
public class BucketFillCommandTest {
    private static final Point INITIAL_POINT = Point.of(1, 1);

    private static final Point UPPER_POINT = Point.of(1, 1);
    private static final Point LOWER_POINT = Point.of(1, 2);
    private static final Point LEFT_POINT = Point.of(2, 1);
    private static final Point RIGHT_POINT = Point.of(2, 2);

    @Test
    public void execute_BucketFillSimpleCanvas() {
        Canvas canvas = new Canvas();
        canvas.initAndFill(2, 2, '_');

        BucketFillCommand command = new BucketFillCommand(canvas, INITIAL_POINT, 'o');
        ExecutionResult executionResult = command.execute();

        assertThat(canvas.getColorAtPoint(UPPER_POINT), is('o'));
        assertThat(canvas.getColorAtPoint(LOWER_POINT), is('o'));
        assertThat(canvas.getColorAtPoint(LEFT_POINT), is('o'));
        assertThat(canvas.getColorAtPoint(RIGHT_POINT), is('o'));

        assertThat(executionResult, is(ExecutionResult.REPAINT));
    }
}
