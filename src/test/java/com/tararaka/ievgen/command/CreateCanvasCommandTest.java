package com.tararaka.ievgen.command;

import com.tararaka.ievgen.Canvas;
import com.tararaka.ievgen.ExecutionResult;
import com.tararaka.ievgen.Dimension;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.mockito.Mockito.verify;

/**
 * @author Ievgen Tararaka
 */
@RunWith(MockitoJUnitRunner.class)
public class CreateCanvasCommandTest {
    @Mock
    private Canvas canvas;

    @Test
    public void execute_InitCanvas_WithWidthHeightAndDefaultColor() {
        CreateCanvasCommand command = new CreateCanvasCommand(canvas, Dimension.of(5, 3));
        ExecutionResult executionResult = command.execute();

        verify(canvas).initAndFill(5, 3, Canvas.DEFAULT_FILL_COLOR);

        assertThat(executionResult, is(ExecutionResult.REPAINT));
    }
}
