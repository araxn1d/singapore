package com.tararaka.ievgen.command;

import com.tararaka.ievgen.BasicDrawLineStrategy;
import com.tararaka.ievgen.Canvas;
import com.tararaka.ievgen.ConsoleReader;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Arrays;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsInstanceOf.instanceOf;
import static org.mockito.Mockito.when;

/**
 * @author Ievgen Tararaka
 */
@RunWith(MockitoJUnitRunner.class)
public class ConsoleCommandFactoryTest {
    @Mock
    private ConsoleReader consoleReader;
    @Mock
    private Canvas canvas;
    @Mock
    private BasicDrawLineStrategy drawLineStrategy;

    private ConsoleCommandFactory commandFactory;

    @Before
    public void setUp() {
        commandFactory = new ConsoleCommandFactory(consoleReader, canvas, drawLineStrategy);
    }

    @Test
    public void getCommand_ReturnCreateCanvasCommand() {
        when(consoleReader.readFromConsole()).thenReturn(Arrays.asList("C", "1", "1"));

        Command command = commandFactory.getCommand();

        assertThat(command, instanceOf(CreateCanvasCommand.class));
    }

    @Test
    public void getCommand_ReturnDrawLineCommand() {
        when(consoleReader.readFromConsole()).thenReturn(Arrays.asList("L", "1", "1", "5", "5"));

        Command command = commandFactory.getCommand();

        assertThat(command, instanceOf(DrawLineCommand.class));
    }

    @Test
    public void getCommand_ReturnDrawRectangleCommand() {
        when(consoleReader.readFromConsole()).thenReturn(Arrays.asList("R", "1", "1", "5", "5"));

        Command command = commandFactory.getCommand();

        assertThat(command, instanceOf(DrawRectangleCommand.class));
    }

    @Test
    public void getCommand_ReturnBucketFillCommand() {
        when(consoleReader.readFromConsole()).thenReturn(Arrays.asList("B", "1", "1", "c"));

        Command command = commandFactory.getCommand();

        assertThat(command, instanceOf(BucketFillCommand.class));
    }

    @Test(expected = CommandCreationException.class)
    public void getCommand_ThrowsException_WhenArgumentsIsNotEnough() {
        when(consoleReader.readFromConsole()).thenReturn(Arrays.asList("B", "1"));

        commandFactory.getCommand();
    }

    @Test(expected = CommandCreationException.class)
    public void getCommand_ThrowsException_WhenArgumentsIsMoreThenNeeded() {
        when(consoleReader.readFromConsole()).thenReturn(Arrays.asList("B", "1", "2", "3", "4"));

        commandFactory.getCommand();
    }
}
