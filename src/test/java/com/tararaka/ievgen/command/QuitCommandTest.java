package com.tararaka.ievgen.command;

import com.tararaka.ievgen.ExecutionResult;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

/**
 * @author Ievgen Tararaka
 */
@RunWith(JUnit4.class)
public class QuitCommandTest {
    @Test
    public void execute_ReturnProgramTermination() {
        QuitCommand command = new QuitCommand();
        assertThat(command.execute(), is(ExecutionResult.TERMINATE));
    }
}
