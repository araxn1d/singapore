package com.tararaka.ievgen.command;

import com.tararaka.ievgen.BasicDrawLineStrategy;
import com.tararaka.ievgen.Canvas;
import com.tararaka.ievgen.ExecutionResult;
import com.tararaka.ievgen.Point;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.mockito.Mockito.verify;

/**
 * @author Ievgen Tararaka
 */
@RunWith(MockitoJUnitRunner.class)
public class DrawLineCommandTest {
    @Mock
    private Canvas canvas;
    @Mock
    private BasicDrawLineStrategy drawLineStrategy;

    @Test
    public void execute_DrawLineBetweenPoints() {
        DrawLineCommand command = new DrawLineCommand(canvas, drawLineStrategy, Point.of(1, 2), Point.of(6, 2));
        ExecutionResult executionResult = command.execute();

        verify(drawLineStrategy).drawLine(canvas, DrawLineCommand.DEFAULT_FILL_COLOR, Point.of(1, 2), Point.of(6, 2));

        assertThat(executionResult, is(ExecutionResult.REPAINT));
    }
}
