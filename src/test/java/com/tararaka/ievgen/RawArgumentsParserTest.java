package com.tararaka.ievgen;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Arrays;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

/**
 * @author Ievgen Tararaka
 */
@RunWith(MockitoJUnitRunner.class)
public class RawArgumentsParserTest {
    @Test
    public void isArgumentsSizeValid_IsTrue_WhenCorrectArgumentsProvided() {
        RawArgumentsParser parser = new RawArgumentsParser(CommandType.BUCKET_FILL, Arrays.asList("1", "1", "c"));
        assertThat(parser.isArgumentsSizeValid(), is(true));
    }

    @Test
    public void nextPoint_ReturnPoint() {
        RawArgumentsParser parser = new RawArgumentsParser(CommandType.BUCKET_FILL, Arrays.asList("1", "1", "c"));

        assertThat(parser.nextPoint(), is(Point.of(1, 1)));
    }

    @Test
    public void nextPoint_ReturnDimension() {
        RawArgumentsParser parser = new RawArgumentsParser(CommandType.BUCKET_FILL, Arrays.asList("1", "1", "c"));

        assertThat(parser.nextDimension(), is(Dimension.of(1, 1)));
    }

    @Test
    public void nextPoint_ReturnColor() {
        RawArgumentsParser parser = new RawArgumentsParser(CommandType.BUCKET_FILL, Arrays.asList("c"));

        assertThat(parser.nextColor(), is('c'));
    }
}
