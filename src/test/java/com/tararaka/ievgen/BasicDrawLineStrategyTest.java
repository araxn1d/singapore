package com.tararaka.ievgen;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

/**
 * @author Ievgen Tararaka
 */
@RunWith(MockitoJUnitRunner.class)
public class BasicDrawLineStrategyTest {
    @Mock
    private Canvas canvas;

    private BasicDrawLineStrategy basicDrawLineStrategy = new BasicDrawLineStrategy();

    @Test
    public void drawLine_FillPointsOnHorizontalLine() {
        basicDrawLineStrategy.drawLine(canvas, '-', Point.of(1, 1), Point.of(5, 1));

        verify(canvas).setColorAtPoint('-', Point.of(1, 1));
        verify(canvas).setColorAtPoint('-', Point.of(2, 1));
        verify(canvas).setColorAtPoint('-', Point.of(3, 1));
        verify(canvas).setColorAtPoint('-', Point.of(4, 1));
        verify(canvas).setColorAtPoint('-', Point.of(5, 1));
    }

    @Test
    public void drawLine_FillPointsOnVerticalLine() {
        basicDrawLineStrategy.drawLine(canvas, '-', Point.of(3, 2), Point.of(3, 5));

        verify(canvas).setColorAtPoint('-', Point.of(3, 2));
        verify(canvas).setColorAtPoint('-', Point.of(3, 3));
        verify(canvas).setColorAtPoint('-', Point.of(3, 4));
        verify(canvas).setColorAtPoint('-', Point.of(3, 5));
    }

    @Test
    public void connectPoints_FillPointsBetweenLines() {
        basicDrawLineStrategy.connectPoints(canvas, '-', Point.of(3, 2), Point.of(3, 5),
                Point.of(1, 5));

        verify(canvas).setColorAtPoint('-', Point.of(3, 2));
        verify(canvas).setColorAtPoint('-', Point.of(3, 3));
        verify(canvas).setColorAtPoint('-', Point.of(3, 4));
        verify(canvas, times(2)).setColorAtPoint('-', Point.of(3, 5));
        verify(canvas).setColorAtPoint('-', Point.of(2, 5));
        verify(canvas).setColorAtPoint('-', Point.of(1, 5));
    }
}
