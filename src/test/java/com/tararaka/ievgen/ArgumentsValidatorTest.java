package com.tararaka.ievgen;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.fail;

/**
 * @author Ievgen Tararaka
 */
@RunWith(JUnit4.class)
public class ArgumentsValidatorTest {
    private static final String MESSAGE = "MESSAGE";

    @Test
    public void assertTrue_ThrowsExceptionWithMessage_WhenConditionIsFalse() {
        try {
            ArgumentsValidator.assertTrue(false, MESSAGE);
            fail("Exception must be thrown");
        } catch (IllegalArgumentException ex) {
            assertThat(ex.getMessage(), is(MESSAGE));
        }
    }

    @Test
    public void assertFalse_ThrowsExceptionWithMessage_WhenConditionIsTrue() {
        try {
            ArgumentsValidator.assertFalse(true, MESSAGE);
            fail("Exception must be thrown");
        } catch (IllegalArgumentException ex) {
            assertThat(ex.getMessage(), is(MESSAGE));
        }
    }
}