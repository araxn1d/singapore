package com.tararaka.ievgen;

/**
 * @author Ievgen Tararaka
 */
public class ConsoleCanvasDrawer implements CanvasDrawer{
    @Override
    public void draw(Canvas canvas) {
        ConsoleFrameCanvasDecorator decorator = new ConsoleFrameCanvasDecorator(canvas);

        for (int y = 1; y <= decorator.getHeight(); y++) {
            for (int x = 1; x <= decorator.getWidth(); x++) {
                System.out.print(decorator.getColorAtPoint(Point.of(x, y)));
            }
            System.out.println();
        }
    }
}
