package com.tararaka.ievgen;

/**
 * @author Ievgen Tararaka
 */
public enum CommandArgumentType {
    DIMENSION(2),
    POINT(2),
    COLOR(1);

    CommandArgumentType(int size) {
        this.size = size;
    }

    private final int size;

    public int getSize() {
        return size;
    }
}
