package com.tararaka.ievgen.command;

import com.tararaka.ievgen.ExecutionResult;

/**
 * @author Ievgen Tararaka
 */
public class QuitCommand implements Command {
    @Override
    public ExecutionResult execute() {
        return ExecutionResult.TERMINATE;
    }
}
