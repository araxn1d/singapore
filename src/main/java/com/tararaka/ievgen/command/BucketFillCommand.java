package com.tararaka.ievgen.command;

import java.util.LinkedList;
import java.util.Queue;

import com.tararaka.ievgen.Canvas;
import com.tararaka.ievgen.ExecutionResult;
import com.tararaka.ievgen.Point;

/**
 * @author Ievgen Tararaka
 */
public class BucketFillCommand extends DefaultDrawCommand {
    private final Point point;
    private final char color;

    BucketFillCommand(Canvas canvas, Point point, char color) {
        super(canvas);
        this.point = point;
        this.color = color;
    }

    @Override
    public ExecutionResult execute() {
        char replacementColor = canvas.getColorAtPoint(point);

        Queue<Point> queue = new LinkedList<>();
        queue.add(point);

        while (!queue.isEmpty()) {
            Point current = queue.remove();
            if (shouldBeFilled(canvas, current, replacementColor)) {
                canvas.setColorAtPoint(color, current);
                // move to 4 directions from current point
                queue.add(Point.of(current.X, current.Y - 1));
                queue.add(Point.of(current.X, current.Y + 1));
                queue.add(Point.of(current.X - 1, current.Y));
                queue.add(Point.of(current.X + 1, current.Y));
            }
        }

        return super.execute();
    }

    private boolean shouldBeFilled(Canvas canvas, Point p, char replacementColor) {
        if (p.X < 1 || p.X > canvas.getWidth()) return false;
        if (p.Y < 1 || p.Y > canvas.getHeight()) return false;
        if (canvas.getColorAtPoint(p) != replacementColor) return false;
        return true;
    }
}
