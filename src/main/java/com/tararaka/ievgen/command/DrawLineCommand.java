package com.tararaka.ievgen.command;

import com.tararaka.ievgen.Canvas;
import com.tararaka.ievgen.ExecutionResult;
import com.tararaka.ievgen.BasicDrawLineStrategy;
import com.tararaka.ievgen.Point;

/**
 * @author Ievgen Tararaka
 */
public class DrawLineCommand extends DefaultDrawCommand {
    public static final char DEFAULT_FILL_COLOR = 'x';

    private final BasicDrawLineStrategy drawLineStrategy;

    private final Point p1;
    private final Point p2;

    DrawLineCommand(Canvas canvas, BasicDrawLineStrategy drawLineStrategy, Point p1, Point p2) {
        super(canvas);
        this.drawLineStrategy = drawLineStrategy;
        this.p1 = p1;
        this.p2 = p2;
    }

    @Override
    public ExecutionResult execute() {
        drawLineStrategy.drawLine(canvas, DEFAULT_FILL_COLOR, p1, p2);
        return super.execute();
    }
}
