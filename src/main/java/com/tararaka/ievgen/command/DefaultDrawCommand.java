package com.tararaka.ievgen.command;

import com.tararaka.ievgen.Canvas;
import com.tararaka.ievgen.ExecutionResult;

/**
 * @author Ievgen Tararaka
 */
public class DefaultDrawCommand implements Command {
    protected Canvas canvas;

    DefaultDrawCommand(Canvas canvas) {
        this.canvas = canvas;
    }

    @Override
    public ExecutionResult execute() {
        return ExecutionResult.REPAINT;
    }
}
