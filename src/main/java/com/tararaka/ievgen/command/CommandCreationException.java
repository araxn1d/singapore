package com.tararaka.ievgen.command;

/**
 * @author Ievgen Tararaka
 */
public class CommandCreationException extends RuntimeException {
    public CommandCreationException(String message) {
        super(message);
    }
}
