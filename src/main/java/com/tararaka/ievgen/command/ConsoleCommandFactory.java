package com.tararaka.ievgen.command;

import com.tararaka.ievgen.*;
import lombok.AllArgsConstructor;

import java.util.List;

/**
 * @author Ievgen Tararaka
 */
@AllArgsConstructor
public class ConsoleCommandFactory implements CommandFactory {
    private final ConsoleReader consoleReader;
    private final Canvas canvas;
    private final BasicDrawLineStrategy drawLineStrategy;

    @Override
    public Command getCommand() {
        List<String> rawArgs = consoleReader.readFromConsole();
        ArgumentsValidator.assertTrue(rawArgs.size() >= 1, "Command was not provided");
        return processArgs(rawArgs);
    }

    private Command processArgs(List<String> rawArgs) {
        CommandType commandType = CommandType.byCombination(rawArgs.get(0));

        RawArgumentsParser parser = new RawArgumentsParser(commandType, rawArgs.subList(1, rawArgs.size()));

        if (!parser.isArgumentsSizeValid()) {
            throw new CommandCreationException("Size of provided arguments is not correct.");
        }

        switch (commandType) {
            case QUIT:
                return new QuitCommand();
            case CREATE_CANVAS:
                return new CreateCanvasCommand(canvas, parser.nextDimension());
            case DRAW_LINE:
                return new DrawLineCommand(canvas, drawLineStrategy, parser.nextPoint(), parser.nextPoint());
            case DRAW_RECTANGLE:
                return new DrawRectangleCommand(canvas, drawLineStrategy, parser.nextPoint(), parser.nextPoint());
            case BUCKET_FILL:
                return new BucketFillCommand(canvas, parser.nextPoint(), parser.nextColor());
            default:
                throw new CommandCreationException("Cannot create command.");
        }
    }
}
