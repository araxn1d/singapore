package com.tararaka.ievgen.command;

import com.tararaka.ievgen.ExecutionResult;

/**
 * Basic interface for all commands.
 *
 * @author Ievgen Tararaka
 */
public interface Command {
    /**
     * Executes command.
     *
     * @return command execution result
     */
    ExecutionResult execute();
}
