package com.tararaka.ievgen.command;

import com.tararaka.ievgen.*;

/**
 * @author Ievgen Tararaka
 */
public class DrawRectangleCommand extends DefaultDrawCommand {
    private static final char DEFAULT_FILL_COLOR = 'x';

    private final BasicDrawLineStrategy drawLineStrategy;

    private final Point p1;
    private final Point p2;

    DrawRectangleCommand(Canvas canvas, BasicDrawLineStrategy drawLineStrategy, Point p1, Point p2) {
        super(canvas);
        this.drawLineStrategy = drawLineStrategy;
        this.p1 = p1;
        this.p2 = p2;
    }

    @Override
    public ExecutionResult execute() {
        Point upperLeft = p1;
        Point upperRight = Point.of(p2.X, upperLeft.Y);
        Point lowerRight = p2;
        Point lowerLeft = Point.of(p1.X, lowerRight.Y);

        drawLineStrategy.connectPoints(canvas, DEFAULT_FILL_COLOR, upperLeft, upperRight, lowerRight, lowerLeft, upperLeft);
        return super.execute();
    }
}
