package com.tararaka.ievgen.command;

import com.tararaka.ievgen.Canvas;
import com.tararaka.ievgen.ExecutionResult;
import com.tararaka.ievgen.Dimension;

/**
 * @author Ievgen Tararaka
 */
public class CreateCanvasCommand extends DefaultDrawCommand {
    private final Dimension dimension;

    CreateCanvasCommand(Canvas canvas, Dimension dimension) {
        super(canvas);
        this.dimension = dimension;
    }

    @Override
    public ExecutionResult execute() {
        canvas.initAndFill(dimension.WIDTH, dimension.HEIGHT, Canvas.DEFAULT_FILL_COLOR);
        return super.execute();
    }
}
