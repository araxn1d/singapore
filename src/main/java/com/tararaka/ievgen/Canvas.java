package com.tararaka.ievgen;

import lombok.NoArgsConstructor;

/**
 * Represents canvas that consist of points that are filled with color.
 *
 * @author Ievgen Tararaka
 */
@NoArgsConstructor
public class Canvas {
    public static final char DEFAULT_FILL_COLOR = ' ';

    private char[][] points;
    private char defaultColor;
    private int width;
    private int height;
    private boolean initialized;

    public void initAndFill(int width, int height, char fillColor) {
        this.defaultColor = fillColor;
        this.width = width;
        this.height = height;
        initialized = true;
        points = new char[height][width];

        fillWithColor();
    }

    public char getColorAtPoint(Point point) {
        validatePoint(point.X, point.Y);
        return points[point.Y - 1][point.X - 1];
    }

    public void setColorAtPoint(char color, Point point) {
        validatePoint(point.X, point.Y);
        points[point.Y - 1][point.X - 1] = color;
    }

    private void fillWithColor() {
        for (int y = 0; y < width; y++) {
            for (int x = 0; x < height; x++) {
                points[x][y] = defaultColor;
            }
        }
    }

    private void validatePoint(int x, int y) {
        ArgumentsValidator.assertFalse(x < 1 || x > width || y < 1 || y > height,
                String.format("Given point is out of canvas bounds. Point(%d,%d)", x, y));
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public boolean isInitialized() {
        return initialized;
    }
}
