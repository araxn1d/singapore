package com.tararaka.ievgen;

/**
 * @author Ievgen Tararaka
 */
public interface CanvasDrawer {
    void draw(Canvas canvas);
}
