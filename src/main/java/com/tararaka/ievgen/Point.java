package com.tararaka.ievgen;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;

/**
 * Simple 2D point (X-axis, Y-axis).
 *
 * @author Ievgen Tararaka
 */
@AllArgsConstructor
@EqualsAndHashCode
public class Point {
    public final int X;
    public final int Y;

    public static Point of(int X, int Y) {
        return new Point(X, Y);
    }
}
