package com.tararaka.ievgen;

/**
 * @author Ievgen Tararaka
 */
public enum ExecutionResult {
    TERMINATE, REPAINT
}
