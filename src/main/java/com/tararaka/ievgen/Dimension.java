package com.tararaka.ievgen;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;

/**
 * @author Ievgen Tararaka
 */
@AllArgsConstructor
@EqualsAndHashCode
public class Dimension {
    public int WIDTH;
    public int HEIGHT;

    public static Dimension of(int width, int height) {
        return new Dimension(width, height);
    }
}
