package com.tararaka.ievgen;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * @author Ievgen Tararaka
 */
public class ConsoleReader {
    private static final String WHITE_SPACE = "\\s";

    public List<String> readFromConsole() {
        try {
            BufferedReader bufferRead = new BufferedReader(new InputStreamReader(System.in));
            String input = bufferRead.readLine();
            return Arrays.asList(input.split(WHITE_SPACE));
        } catch (IOException e) {
            return Collections.emptyList();
        }
    }
}
