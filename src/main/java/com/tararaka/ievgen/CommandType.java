package com.tararaka.ievgen;

import com.tararaka.ievgen.command.CommandCreationException;

import java.util.stream.Stream;

import static com.tararaka.ievgen.CommandArgumentType.*;

/**
 * @author Ievgen Tararaka
 */
public enum CommandType {
    QUIT("Q"),
    CREATE_CANVAS("C", DIMENSION),
    DRAW_LINE("L", POINT, POINT),
    DRAW_RECTANGLE("R", POINT, POINT),
    BUCKET_FILL("B", POINT, COLOR);

    private final String commandCombination;
    private final CommandArgumentType[] arguments;

    CommandType(String commandCombination, CommandArgumentType... arguments) {
        this.commandCombination = commandCombination;
        this.arguments = arguments;
    }

    public static CommandType byCombination(String combination) {
        return Stream.of(values()).filter(it -> combination.equals(it.commandCombination))
                .findFirst()
                .orElseThrow(() -> new CommandCreationException(
                        String.format("Cannot define command type by provided combination %s.", combination)));
    }

    public String getCommandCombination() {
        return commandCombination;
    }

    public CommandArgumentType[] getArguments() {
        return arguments;
    }
}
