package com.tararaka.ievgen;

import com.tararaka.ievgen.command.Command;
import com.tararaka.ievgen.command.ConsoleCommandFactory;

import static com.tararaka.ievgen.ExecutionResult.REPAINT;
import static com.tararaka.ievgen.ExecutionResult.TERMINATE;

/**
 * @author Ievgen Tararaka
 */
public class ConsoleCanvas {
    public static void main(String[] args) {
        try {
            runApplication();
        } catch (Exception e) {
            System.out.println("Unexpected exception raised during execution.");
            e.printStackTrace(System.out);
            System.out.println("Program will be terminated.");
        }
    }

    private static void runApplication() {
        ConsoleReader consoleReader = new ConsoleReader();
        BasicDrawLineStrategy drawLineStrategy = new BasicDrawLineStrategy();
        CanvasDrawer canvasDrawer = new ConsoleCanvasDrawer();
        Canvas canvas = new Canvas();

        CommandFactory commandFactory = new ConsoleCommandFactory(consoleReader, canvas, drawLineStrategy);
        ExecutionResult executionResult;

        do {
            System.out.print("enter command: ");

            Command command = commandFactory.getCommand();
            executionResult = command.execute();

            if (executionResult == REPAINT) canvasDrawer.draw(canvas);

        } while (executionResult != TERMINATE);
    }
}
