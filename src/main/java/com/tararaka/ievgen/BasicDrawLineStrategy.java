package com.tararaka.ievgen;

/**
 * @author Ievgen Tararaka
 */
public class BasicDrawLineStrategy {
    /**
     * Connects all given points by drawing line with given color.
     * Points are connected from left to right, one by one.
     * <p>
     * Only horizontal or vertical lines between points could be drawn, other types of lines will be skipped.
     * </p>
     *
     * @param canvas where points should be connected
     * @param color  color of lines
     * @param points points to connect
     */
    public void connectPoints(Canvas canvas, char color, Point... points) {
        ArgumentsValidator.assertTrue(points.length >= 2,
                String.format("Need 2 or more points to connect, but given %d points", points.length));

        Point previous = points[0];

        for (int i = 1; i < points.length; i++) {
            Point current = points[i];
            drawLine(canvas, color, previous, current);
            previous = current;
        }
    }

    /**
     * Draw line between 2 points in canvas with given color.
     * <p>
     * Only horizontal or vertical lines could be drawn, other types of lines will be skipped.
     * </p>
     *
     * @param canvas where line should be drawn
     * @param color  color of line
     * @param p1     beginning of the line
     * @param p2     end of the line
     */
    public void drawLine(Canvas canvas, char color, Point p1, Point p2) {
        if (p1.X == p2.X) {
            drawVerticalLine(canvas, color, p1, p2);
        }
        if (p1.Y == p2.Y) {
            drawHorizontalLine(canvas, color, p1, p2);
        }
    }

    private void drawHorizontalLine(Canvas canvas, char color, Point p1, Point p2) {
        Line line = getStartingPoint(p1, p1.X, p2, p2.X);
        for (int x = line.from.X; x <= line.to.X; x++) {
            canvas.setColorAtPoint(color, Point.of(x, line.from.Y));
        }
    }

    private void drawVerticalLine(Canvas canvas, char color, Point p1, Point p2) {
        Line line = getStartingPoint(p1, p1.Y, p2, p2.Y);
        for (int y = line.from.Y; y <= line.to.Y; y++) {
            canvas.setColorAtPoint(color, Point.of(line.from.X, y));
        }
    }

    private Line getStartingPoint(Point p1, int coordinate1, Point p2, int coordinate2) {
        if (coordinate1 <= coordinate2) {
            return new Line(p1, p2);
        }
        return new Line(p2, p1);
    }

    private class Line {
        private Point from;
        private Point to;

        private Line(Point from, Point to) {
            this.from = from;
            this.to = to;
        }
    }
}
