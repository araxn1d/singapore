package com.tararaka.ievgen;

/**
 * Class is a validator for methods arguments.
 *
 * @author Ievgen Tararaka
 */
public class ArgumentsValidator {
    /**
     * Asserts that condition is <code>true</code>. Otherwise throws exception with given message.
     *
     * @param condition condition that should be asserted
     * @param message   exception message
     * @throws IllegalArgumentException exception with given message
     */
    public static void assertTrue(boolean condition, String message) throws IllegalArgumentException {
        if (!condition) throw new IllegalArgumentException(message);
    }

    /**
     * Asserts that condition is <code>false</code>. Otherwise throws exception with given message.
     *
     * @param condition condition condition that should be asserted
     * @param message   exception message
     * @throws IllegalArgumentException exception with given message
     */
    public static void assertFalse(boolean condition, String message) throws IllegalArgumentException {
        if (condition) throw new IllegalArgumentException(message);
    }
}
