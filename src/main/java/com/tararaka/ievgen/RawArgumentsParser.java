package com.tararaka.ievgen;

import java.util.List;

/**
 * @author Ievgen Tararaka
 */
public class RawArgumentsParser {
    private final CommandType commandType;
    private final List<String> rawArguments;

    private int rawArgsIndex = 0;

    public RawArgumentsParser(CommandType commandType, List<String> rawArguments) {
        this.commandType = commandType;
        this.rawArguments = rawArguments;
    }

    /**
     * Verifies that provided arguments size is corresponds to command type.
     *
     * @return indication flag
     */
    public boolean isArgumentsSizeValid() {
        int totalRequiredSize = 0;
        for (CommandArgumentType commandArgumentType : commandType.getArguments()) {
            totalRequiredSize += commandArgumentType.getSize();
        }
        return totalRequiredSize == rawArguments.size();
    }

    /**
     * Get point from provided arguments.
     *
     * @return point
     */
    public Point nextPoint() {
        Point p = Point.of(asInt(rawArguments.get(rawArgsIndex)), asInt(rawArguments.get(rawArgsIndex + 1)));
        rawArgsIndex += 2;
        return p;
    }

    /**
     * Get dimension from provided arguments.
     *
     * @return dimension
     */
    public Dimension nextDimension() {
        Dimension d = Dimension.of(asInt(rawArguments.get(rawArgsIndex)), asInt(rawArguments.get(rawArgsIndex + 1)));
        rawArgsIndex += 2;
        return d;
    }

    /**
     * Get color from provided arguments.
     *
     * @return color
     */
    public char nextColor() {
        char c = asChar(rawArguments.get(rawArgsIndex));
        rawArgsIndex++;
        return c;
    }

    private int asInt(String value) {
        return Integer.valueOf(value);
    }

    private char asChar(String value) {
        return value.charAt(0);
    }
}
