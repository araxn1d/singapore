package com.tararaka.ievgen;

import com.tararaka.ievgen.command.Command;

/**
 * @author Ievgen Tararaka
 */
public interface CommandFactory {
    Command getCommand();
}
