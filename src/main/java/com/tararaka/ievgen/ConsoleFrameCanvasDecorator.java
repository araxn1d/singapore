package com.tararaka.ievgen;

/**
 * @author Ievgen Tararaka
 */
class ConsoleFrameCanvasDecorator extends Canvas {
    private Canvas canvas;

    ConsoleFrameCanvasDecorator(Canvas canvas) {
        this.canvas = canvas;
    }

    /**
     * Decorates canvas and create border over it.
     * <p>
     * Return <code>-</code>, <code>|</code> depending on point coordinates.
     * </p>
     *
     * @param point point to get color of
     * @return color of point,
     */
    @Override
    public char getColorAtPoint(Point point) {
        if (point.Y == 1 || point.Y == canvas.getHeight() + 2) {
            return '-';
        } else if (point.X == 1 || point.X == canvas.getWidth() + 2) {
            return '|';
        } else {
            return canvas.getColorAtPoint(Point.of(point.X - 1, point.Y - 1));
        }
    }

    @Override
    public void initAndFill(int width, int height, char fillColor) {
        canvas.initAndFill(width, height, fillColor);
    }

    @Override
    public boolean isInitialized() {
        return canvas.isInitialized();
    }

    @Override
    public int getWidth() {
        return canvas.getWidth() + 2;
    }

    @Override
    public int getHeight() {
        return canvas.getHeight() + 2;
    }
}
